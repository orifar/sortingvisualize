import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

import unit4.collectionsLib.Queue;

@SuppressWarnings("serial")
class SortingGraphics extends JFrame {

    SortingGraphics(int WIDTH, int HEIGHT, int panelWidth, int panelHeight, int DELAY) {
        setTitle("my frame");
        DrawPane pane = new DrawPane(WIDTH, HEIGHT, panelWidth, panelHeight, DELAY);
        this.getContentPane().add(pane);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(panelWidth, panelHeight);
        setResizable(true);
        setVisible(true);
        pane.start();
    }

    class DrawPane extends JPanel {
        private final int WIDTH, HEIGHT, panelWidth, panelHeight, DELAY;
        private int[][] nums;

        DrawPane(int WIDTH, int HEIGHT, int panelWidth, int panelHeight, int DELAY) {
            this.WIDTH = WIDTH;
            this.HEIGHT = HEIGHT;
            this.panelHeight = panelHeight;
            this.panelWidth = panelWidth;
            this.DELAY = DELAY;
            nums = new int[HEIGHT][WIDTH];

        }

        void start() {
            for (int i = 0; i < this.HEIGHT; i++) {
                for (int j = 0; j < this.WIDTH; j++) {
                    nums[i][j] = j;
                }
            }
            SortingMain.shuffle(nums);
            repaint();
            radixLSDSort();
        }

        public void paintComponent(Graphics g) {
            //Paint stuff
            super.paintComponent(g);
            for (int i = 0; i < HEIGHT; i++) {
                for (int j = 0; j < WIDTH; j++) {
                    g.setColor(SortingMain.getRGB(nums[i][j]));
                    g.fillRect(j * (panelWidth / WIDTH), i * (panelHeight / HEIGHT), panelWidth / WIDTH, panelHeight / HEIGHT);
                }
            }
        }

        /*
         * Bubble sort for a 2D array
         */
        public void bubbleSort() {
            int switches = 1, columnsDone = 0, temp;
            //Keep sorting as long as
            while (switches > 0) {
                switches = 0;
                //Go over
                for (int j = 0; j < WIDTH - columnsDone - 1; j++) {
                    //Go over rows
                    for (int i = 0; i < HEIGHT - 1; i++) {
                        //Switch two blocks if the left one is larger than the right
                        if (nums[i][j] > nums[i][j + 1]) {
                            temp = nums[i][j];
                            nums[i][j] = nums[i][j + 1];
                            nums[i][j + 1] = temp;
                            switches++;
                        }
                        repaint();
                    }
                    try {
                        Thread.sleep(DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                columnsDone++;
            }
        }

        private void insertionSort() {
            int temp;
            boolean switched;
            for (int i = 1; i < WIDTH; i++) {
                switched = true;
                for (int g = i; g > 0 && switched; g--) {
                    switched = false;
                    for (int j = 0; j < HEIGHT; j++) {
                        if (nums[g][j] < nums[g - 1][j]) {
                            temp = nums[g][j];
                            nums[g][j] = nums[g - 1][j];
                            nums[g - 1][j] = temp;
                            switched = true;
                            repaint();
                        }
                    }
                    //Delay
                    try {
                        Thread.sleep(DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        private void radixLSDSort() {
            Queue<Integer>[][] queues = new Queue[HEIGHT][10];
            for (int i = 0; i < HEIGHT; i++) {
                for (int j = 0; j < 10; j++) {
                    queues[i][j] = new Queue<Integer>();
                }
            }
            int numOfDigits = (int) Math.log10(WIDTH - 1) + 1;
            for (int currDigit = 1; currDigit <= numOfDigits; currDigit++) {
                for (int j = 0; j < HEIGHT; j++) {
                    for (int g = 0; g < WIDTH; g++) {
                        int tens = (int) Math.pow(10, currDigit - 1);
                        int currNum = nums[j][g];
                        int relevantDigit = (currNum / tens) % 10;
                        queues[j][relevantDigit].insert(currNum);
                    }
                    System.out.println(j);
                }
                /*for (int j = 0; j < HEIGHT; j++) {
                    int currWidth = 0;
                    for (int i = 0; i < 10; i++) {
                        while (!queues[j][i].isEmpty()) {
                            nums[j][currWidth] = queues[j][i].remove();

                            currWidth++;
                        }
                        repaint();

                    }
                    try {
                        Thread.sleep(DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }*/
                for(int i=0;i<WIDTH;i++)
                {
                    for(int j = 0; j<HEIGHT; j++)
                    {
                        int q = 0;
                        while(queues[j][q].isEmpty())
                        {
                            q++;
                        }
                        nums[j][i] = queues[j][q].remove();
                    }
                    repaint();
                    try {
                        Thread.sleep(DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }
}