
public class Test {
	public static void main(String[] args)
	{
		int HEIGHT = 10, WIDTH = 10, temp =0;
		int[][] nums = new int[WIDTH][HEIGHT];
		// TODO Auto-generated method stub
		for(int i = 0; i<HEIGHT;i++)
    	{
    		for(int j= 0; j<WIDTH; j++)
    		{
    			nums[j][i] = j;
    		}
    	}
		SortingMain.shuffle(nums);
		for(int i = 1;i<WIDTH;i++)
		{
			for(int j =0;j<HEIGHT;j++)
			{
				for(int g=i;g>0&&nums[g][j]<nums[g-1][j];g--)
				{
					if(nums[g][j]<nums[g-1][j])
					{
						temp = nums[g][j];
						nums[g][j] = nums[g-1][j];
						nums[g-1][j] = temp;
					}
					printArray(nums,WIDTH,HEIGHT);
					System.out.println("\n");
				}
			}
		}
	}
	public static void printArray(int[][] arr, int WIDTH, int HEIGHT)
	{
		for(int i = 0; i<WIDTH;i++)
		{
			for(int j=0; j<HEIGHT;j++)
			{
				System.out.print(arr[j][i]);
			}
			System.out.println();
		}
	}
}
