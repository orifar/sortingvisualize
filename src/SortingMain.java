import java.util.Random;
import java.awt.Color;
public class SortingMain {
	private final static int panelWidth = 1536, panelHeight = 864, WIDTH = 153, HEIGHT =86 ,  DELAY = 100;
	public static void main(String[] args) {
	    @SuppressWarnings("unused")
		SortingGraphics SG = new SortingGraphics(WIDTH, HEIGHT, panelWidth, panelHeight, DELAY);    
	}
	/*
	 * Shuffles a 2D array
	 * Input: the array to be shuffled
	 * Output: none
	 */
	public static void shuffle(int array[][])
	{
		int index, temp;
		Random random = new Random();
		for(int i = 0; i<array.length; i++)
		{
			for (int j = array[0].length - 1; j > 0; j--)
			{
				index = random.nextInt(j + 1);
				temp = array[i][index];
				array[i][index] = array[i][j];
				array[i][j] = temp;
			}
		}
	}
	/*
	 * 
	 */
	public static Color getRGB(float i)
	{
		int color = Color.HSBtoRGB((float)(i/WIDTH), (float)1, (float)0.75);
		int red = 0, green = 0, blue = 0;
		red = (color>>16)&0xFF;
		green = (color>>8)&0xFF;
		blue = color&0xFF;
		return (new Color(red,green,blue));
	}
}